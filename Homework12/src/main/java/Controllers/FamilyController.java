package Controllers;

import Entitiy.*;
import Services.FamilyService;


import java.util.List;


public class FamilyController {
    FamilyService familyService ;

    public FamilyController() {
        familyService = new FamilyService();
    }

    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
       familyService.displayAllFamilies();
    }
    public void getFamiliesBiggerThan(int number){
        System.out.println("Test");
       familyService.getFamiliesBiggerThan(number);

    }
    public void getFamiliesLessThan(int number){
      familyService.getFamiliesLessThan(number);

    }
    public int countFamiliesWithMemberNumber(int number){
     return familyService.countFamiliesWithMemberNumber(number);
    }

    public  boolean createNewFamily(Human mother, Human father){
        return  familyService.createNewFamily(mother, father);

    }
    public boolean deleteFamilyByIndex(int index){
        try {
            return familyService.deleteFamilyByIndex(index-1);
        } catch (Exception e){
            System.out.println("Wrong Index!!!!!");
        }
       return false;
    }
    public Family adoptChild(int id, String name, String surname, String year, int iq){
        Human child = new Human(name, surname, year, iq);
        return familyService.adoptChild(getAllFamilies().get(id-1), child);
    }

    public void deleteAllChildrenOlderThen(int number) {
      familyService.deleteAllChildrenOlderThen(number);
    }

    public int count(){
        return familyService.count();
    }
    public  Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public List<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet){
        familyService.addPet(index, pet);
    }



    public Family bornChild(int id, String feminine, String masculine){

        return  familyService.bornChild(getAllFamilies().get(id-1), feminine, masculine);

    }

public  boolean saveFamily(Family family){
        return  familyService.addFamily(family);
}
    public  void createRandomFamily(){
        familyService.createRandomFamily();
    }


    public boolean createNewFamilyWithParameters(String m_name, String m_surname, String m_year, int m_iq,String f_name, String f_surname, String f_year, int f_iq){
     return   familyService.createNewFamilyWithParameters( m_name,  m_surname,  m_year,  m_iq, f_name,  f_surname,  f_year,  f_iq);
    }
}
