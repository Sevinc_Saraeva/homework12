package Controllers;

import Console.Console;
import Entitiy.MenuItems;

import java.util.Scanner;

public class MainController {
        Console console;
    public MainController(Console console) {
        this.console = console;
    }

    public void  run() {
        FamilyController familyController = new Controllers.FamilyController();
        Scanner in = new Scanner(System.in);


        boolean start = true;
        while (start) {
            MenuItems.showMainMenu();
            String index = in.next();
            switch (index) {
                case "1":
                    createRandomFamily(familyController);
                    break;
                case "2":
                    displayAllFamilies(familyController);
                    break;
                case "3":
                    displayAllFamiliesMoreThan(familyController);
                    break;
                case "4":
                    displayAllFamiliesLessThan(familyController);
                    break;
                case "5":
                    calculateFamilies(familyController);
                    break;
                case "6":
                    createNewFamily(familyController);
                    break;
                case "7":
                    deleteFamilyByIndex(familyController);
                    break;
                case "8":
                    editFamilyByIndex(familyController);
                    break;
                case "9":
                    removeChildren(familyController);
                    break;
                case "10":
                    start = false;
                    break;
            }
        }
    }
    public  void createRandomFamily(FamilyController familyController){
        familyController.createRandomFamily();
    }

    public  void displayAllFamilies(FamilyController familyController){
        familyController.displayAllFamilies();
    }

    public  void  displayAllFamiliesMoreThan(FamilyController familyController){
        System.out.println("Please, enter the number: ");
        familyController.getFamiliesBiggerThan(checkInputIsInteger(console.readLn()));
    }

    public  void  displayAllFamiliesLessThan(FamilyController familyController){
        System.out.println("Please, enter the number: ");
        familyController.getFamiliesLessThan(checkInputIsInteger(console.readLn()));
    }

    public  void   calculateFamilies(FamilyController familyController){
        System.out.println("Please, enter the number: ");
        System.out.println(familyController.countFamiliesWithMemberNumber(checkInputIsInteger(console.readLn())));

    }

    public  void   createNewFamily(FamilyController familyController ){
        System.out.println("Please, enter the mother's name : ");
        String m_name = console.readLn();
        System.out.println("Please, enter the mother's surname : ");
        String m_surname = console.readLn();
        System.out.println("Please, enter the mother's birth year : ");
        String m_year = console.readLn();
        System.out.println("Please, enter the mother's month of birth : ");
        String m_month = console.readLn();
        System.out.println("Please, enter the mother's birthday : ");
        String m_day = console.readLn();
        System.out.println("Please, enter the mother's iq : ");
        int iq = checkInputIsInteger(console.readLn());

        System.out.println("Please, enter the father's name : ");
        String f_name = console.readLn();
        System.out.println("Please, enter the father's surname : ");
        String f_surname =console.readLn();
        System.out.println("Please, enter the father's birth year : ");
        String f_year = console.readLn();
        System.out.println("Please, enter the father's month of birth : ");
        String f_month = console.readLn();
        System.out.println("Please, enter the father's birthday : ");
        String f_day = console.readLn();
        System.out.println("Please, enter the father's iq : ");
        int f_iq = checkInputIsInteger(console.readLn());
        if(familyController.createNewFamilyWithParameters(m_name,m_surname,m_day+"/"+m_month+"/"+m_year, iq, f_name, f_surname, f_day+"/"+f_month+"/"+f_year, f_iq)){
            System.out.println("Family created successfully");
        }
        else System.out.println("Something went wrong!");

    }

    public  void   deleteFamilyByIndex(FamilyController familyController){
        System.out.println("Please, enter the number: ");
        familyController.deleteFamilyByIndex(checkInputIsInteger(console.readLn()));
    }

    public  void   editFamilyByIndex(FamilyController familyController ){
        System.out.println("1.  Give birth to a baby ");
        System.out.println("2.  Adopt a child ");
        String index =console.readLn();
        switch (index){
            case "1":
            System.out.println("Please, enter the id of family: ");
            int id = checkInputIsInteger(console.readLn());
            System.out.println("Please, enter the girl name : (If you dant want tap tp Enter)  ");
            String feminien = console.readLn();
            System.out.println("Please, enter the boy name :   (If you dant want tap tp Enter) ");
            String masculen = console.readLn();
            familyController.bornChild(id, feminien, masculen);
            break;
            case "2":
                System.out.println("Please, enter the id of family: ");
                int id_2 = checkInputIsInteger(console.readLn());
                System.out.println("Please, enter the name : ");
                String name = console.readLn();
                System.out.println("Please, enter the surname: ");
                String surname = console.readLn();
                System.out.println("Please, enter the year of born : ");
                String year = console.readLn();
                System.out.println("Please, enter the month of born : ");
                String month = console.readLn();
                System.out.println("Please, enter the day of born : ");
                String day = console.readLn();
                System.out.println("Please, enter the intelligence of child : ");
                int iq =checkInputIsInteger(console.readLn());
                familyController.adoptChild(id_2, name, surname, day+"/"+month+"/"+year, iq);
        }
    }

    public  void   removeChildren(FamilyController familyController ){
        System.out.println("Please, enter the age which you want delete : ");
        int age = checkInputIsInteger(console.readLn());
        familyController.deleteAllChildrenOlderThen(age);
    }
    private  int checkInputIsInteger(String input) {
        int id ;
        while (true) {
            try {
                id = Integer.parseInt(input);
                break;
            } catch (Exception e) {
                console.printLn("PLEASE ENTER A RIGHT INPUT : ");
                input = console.readLn();
            }
        }
        return id;
    }

}



