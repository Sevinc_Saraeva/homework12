package Exceptions;


public class FamilyOverflowException extends RuntimeException {
    private String reason;

    public FamilyOverflowException(String reason) {
        super(reason);
        this.reason = reason;
    }
}
