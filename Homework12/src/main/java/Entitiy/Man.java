package Entitiy;
public class Man extends Human {


    public Man() {
        super();
    }

    public Man(String name, String surname, String birthdate) {
        super(name, surname, birthdate);
    }

    public Man(String name, String surname, String birthdate, Family family) {
        super(name, surname, birthdate, family);
    }
    public Man(String name, String surname, String birthdate, int iq) {
      super(name, surname, birthdate, iq);
    }
    public void repairCar(){
        System.out.println(super.getName()+ " is repair car");
    }

    @Override
    public void greetPet() {
        System.out.println("hello, " + super.getFamily().getPet().get(0).getNickname());
    }
}
