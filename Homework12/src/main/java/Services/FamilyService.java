package Services;

import DAO.CollectionFamilyDao;
import Entitiy.*;
import Exceptions.FamilyOverflowException;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService() {
        collectionFamilyDao = new CollectionFamilyDao();
 }

 public List<Family> getAllFamilies(){
        return  collectionFamilyDao.getAllFamilies();
 }

 public void displayAllFamilies(){
        List<Family> families = getAllFamilies();
        if(getAllFamilies().size()==0) System.out.println("There is no any family");
        else
     for (int i = 0; i < families.size(); i++) {
         System.out.println(i+1+". "+families.get(i).toString());
     }
 }
 public void getFamiliesBiggerThan(int number){
     getAllFamilies().stream().filter(family -> family.countFamily()>number).forEach(family -> System.out.println(family.toString()));


 }
    public void getFamiliesLessThan(int number){
        getAllFamilies().stream().filter(family -> family.countFamily()<number).forEach(family -> System.out.println(family.toString()));

    }
    public int countFamiliesWithMemberNumber(int number){
        List<Family> collect = getAllFamilies().stream().filter(family -> family.countFamily() == number).collect(Collectors.toList());
    return  collect.size();
    }

public  boolean createNewFamily(Human mother, Human father){
        return  collectionFamilyDao.saveFamily(new Family(mother, father));

}
public boolean deleteFamilyByIndex(int index){
        return collectionFamilyDao.deleteFamily(index);
}
public Family adoptChild(Family family, Human child){
    for (int i = 0; i < getAllFamilies().size(); i++) {
        if(getAllFamilies().get(i).equals(family)){
            family.addChild(child);
            getAllFamilies().remove(family);
            getAllFamilies().add(family);
        }
        return family;
    }
    throw  new RuntimeException("No family found");
}

public void deleteAllChildrenOlderThen(int number) {
    for (Family family: getAllFamilies()) {
       List<Human> collected=  family.getChildren().stream().filter(child-> Year.now().getValue()- Instant.ofEpochMilli(child.getBirthdate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() > number).collect(Collectors.toList());
          collected.forEach(child -> family.deleteChild(child));
        System.out.println("test");
    }
}

public int count(){
        return  getAllFamilies().size();
}
public  Family getFamilyById(int index){
        return getAllFamilies().get(index);
}
    public List<Pet> getPets(int index){
        return  getAllFamilies().get(index).getPet();
    }

    public boolean addPet(int index, Pet pet){
        Family fam =  getAllFamilies().get(index);
        fam.getPet().add(pet);
        getAllFamilies().set(index,fam);
        return true;
    }



public Family bornChild(Family family, String feminine, String masculine){
    for (int i = 0; i < getAllFamilies().size(); i++) {
        if (getAllFamilies().get(i).equals(family)) {
            if(family.getChildren().size()>4) throw new FamilyOverflowException("Family size can not be larger than 6. ");
            if (feminine.equals("")) {
                getAllFamilies().remove(family);
                family.addChild(new Man(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
            } else if (masculine.equals("")) {
                family.addChild(new Woman (feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
            } else {
                family.addChild(new Man(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
                family.addChild(new Woman(feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
            }
        }

    }
    getAllFamilies().add(family);
        return  family;

}
public  boolean addFamily(Family family){
        return collectionFamilyDao.saveFamily(family);
}

public  String prettyFormat(int k){
        return getAllFamilies().get(k).prettyFormat();

}
public  List<String> prettyFormat(){
    List<String> collect = getAllFamilies().stream().map(x -> x.prettyFormat()).collect(Collectors.toList());
    return collect;
}


    public  void createRandomFamily(){
        Random random = new Random();
        int family_number = random.nextInt(5)+1;
        for (int i = 0; i < family_number; i++) {
            Woman mother = new Woman(WomanNames.values()[random.nextInt(WomanNames.values().length - 1)].get(), "AAAA", LocalDate.now().minusYears(random.nextInt(60)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
            Man father = new Man(ManNames.values()[random.nextInt(ManNames.values().length - 1)].get(), "AAAA", LocalDate.now().minusYears(random.nextInt(60)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
            mother.setIq_level(random.nextInt(200));
            father.setIq_level(random.nextInt(200));
            Family family = new Family(mother, father);
            int childNumber = random.nextInt(4);
            for (int j = 0; j < childNumber; j++) {
                int flag = random.nextInt(2);
                if (flag == 0) {
                    Woman baby_girl = new Woman(WomanNames.values()[random.nextInt(WomanNames.values().length - 1)].get(), "AAA", LocalDate.now().minusYears(random.nextInt(20)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
                    baby_girl.setIq_level(random.nextInt(200));
                    family.addChild(baby_girl);
                } else {
                    Man baby_boy = new Man(ManNames.values()[random.nextInt(ManNames.values().length - 1)].get(), "AAA", LocalDate.now().minusYears(random.nextInt(20)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
                    family.addChild(baby_boy);
                    baby_boy.setIq_level(random.nextInt(200));
                }
            }
            collectionFamilyDao.saveFamily(family);
        }
    }
 public  Man createNewFather(String name, String surname, String year, int iq){
        return new Man(name, surname, year, iq);
 }
    public  Woman createNewMother(String name, String surname, String year, int iq){
        return new Woman (name, surname, year, iq);
    }
    public  boolean  createNewFamilyWithParameters(String m_name, String m_surname, String m_year, int m_iq,String f_name, String f_surname, String f_year, int f_iq){
      return createNewFamily(createNewMother(m_name, m_surname, m_year, m_iq), createNewFather(f_name, f_surname, f_year, f_iq));
    }
}