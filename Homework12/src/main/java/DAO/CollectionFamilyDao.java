package DAO;
import Entitiy.Family;

import java.util.ArrayList;
import java.util.List;


public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;

    public CollectionFamilyDao() {
        families = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        } else throw new RuntimeException("There is no any family at that index");
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        } else return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (family != null && family instanceof Family) {
            return families.remove(family);
        } else throw new RuntimeException("family object is not right");
    }

    @Override
    public boolean saveFamily(Family family) {
        return families.add(family);
    }


    public boolean saveAllFamilies(List<Family> families2) {
        if (families2.size() != 0) {
            families = families2;
            return true;
        }
        return false;

    }
}
