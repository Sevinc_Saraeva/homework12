
import Controllers.MainController;
import Console.SystemConsole;

public class Main {

    public static void main(String[] args) {
        SystemConsole console = new SystemConsole();
        MainController mainController = new MainController(console);
        mainController.run();
    }
}
