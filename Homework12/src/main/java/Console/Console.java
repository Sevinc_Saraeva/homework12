package Console;

public interface Console {
    void printLn(Object s);

    String readLn();
}
